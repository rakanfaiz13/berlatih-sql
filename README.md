# Berlatih SQL

<p>Cakupan Pengerjaan tugas SQL : </p>
<ol>
    <li>Membuat Database</li>
    <li>Membuat Table di Dalam Database</li>
    <li>Memasukan Data pada Table</li>
    <li>Mengambil Data dari Database</li>
    <li>Mengubah Data dari Database</li>
</ol>